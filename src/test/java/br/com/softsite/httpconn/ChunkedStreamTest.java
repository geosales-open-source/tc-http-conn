package br.com.softsite.httpconn;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.junit.Test;

import br.com.softsite.httpconn.stream.ChunkedInputStream;
import br.com.softsite.httpconn.stream.ChunkedOutputStream;
import br.com.softsite.toolbox.indirection.Indirection;
import totalcross.util.IOUtils;

public class ChunkedStreamTest {

	@Test
	public void leituraChunked() throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Indirection<String> ind = new Indirection<>("falhou");
		String leituraChunked = "7\r\n" + 
				"Mozilla\r\n" + 
				"9\r\n" + 
				"Developer\r\n" + 
				"7\r\n" + 
				"Network\r\n" + 
				"0\r\n" + 
				"\r\n";
		ChunkedInputStream chunkedRead = new ChunkedInputStream(new ByteArrayInputStream(leituraChunked.getBytes()), () -> ind.x = "ok");
		IOUtils.copy(chunkedRead, baos, 100);
		assertEquals("MozillaDeveloperNetwork", baos.toString());
		assertEquals("ok", ind.x);
	}

	@Test
	public void escritaChunked() throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try (ChunkedOutputStream chunkedOutputStream = new ChunkedOutputStream(baos)) {
			chunkedOutputStream.write("Mozilla".getBytes());
			chunkedOutputStream.write("Developer".getBytes());
			chunkedOutputStream.write("Network".getBytes());
		}
		String leituraChunked = baos.toString();
		assertEquals("7\r\n" +
				"Mozilla\r\n" +
				"9\r\n" +
				"Developer\r\n" +
				"7\r\n" +
				"Network\r\n" +
				"0\r\n" +
				"\r\n", leituraChunked);
	}
}
