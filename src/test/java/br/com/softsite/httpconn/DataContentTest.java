package br.com.softsite.httpconn;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;

import br.com.softsite.httpconn.conn.ConnPool;
import br.com.softsite.httpconn.message.Headers;
import br.com.softsite.httpconn.message.HttpRequest;
import br.com.softsite.httpconn.message.HttpRequestBuilder;
import br.com.softsite.httpconn.message.HttpResponse;
import br.com.softsite.httpconn.url.Url;
import br.com.softsite.toolbox.Pair;
import br.com.softsite.toolbox.collection.CollectionUtils;
import br.com.softsite.toolbox.indirection.IntIndirection;
import totalcross.util.IOUtils;

public class DataContentTest {

	private static final Url testUrl = new Url("http://httpbin.org");
	private static final String[] mdnValues = {"Mozilla", "Developer", "Network"};
	private Supplier<byte[]> mdnSupplier;

	@Before
	public void criaSupplier() {
		IntIndirection i = new IntIndirection(0);
		mdnSupplier = () -> {
			int x = i.x;
			i.x++;

			if (x >= mdnValues.length) {
				return null;
			}
			return mdnValues[x].getBytes();
		};
	}

	@Test
	public void naoContemDados() {
		HttpRequest httpReq = new HttpRequest(testUrl, testUrl.host, testUrl.path, HttpMethod.GET.toString(), "1.1", new ArrayList<>());
		assertFalse(httpReq.hasData());
	}

	@Test
	public void naoContemDadosBodyNull() {
		byte[] body = null;
		HttpRequest httpReq = new HttpRequest(testUrl, testUrl.host, testUrl.path, HttpMethod.GET.toString(), "1.1", new ArrayList<>(), body);
		assertFalse(httpReq.hasData());
	}

	@Test
	public void naoContemDadosBodySupplierNull() {
		Supplier<byte[]> body = null;
		HttpRequest httpReq = new HttpRequest(testUrl, testUrl.host, testUrl.path, HttpMethod.GET.toString(), "1.1", new ArrayList<>(), body);
		assertFalse(httpReq.hasData());
	}

	@Test
	public void naoContemDadosBodySupplierSemConteudo() {
		Supplier<byte[]> body = () -> null;
		HttpRequest httpReq = new HttpRequest(testUrl, testUrl.host, testUrl.path, HttpMethod.GET.toString(), "1.1", new ArrayList<>(), body);
		assertFalse(httpReq.hasData());
	}

	@Test
	public void contemDadosBodySupplierComConteudo() throws IOException {
		Supplier<byte[]> body = mdnSupplier;
		Pair<String, String> contentLength = Pair.getPair(Headers.KEYS.CONTENT_LENGTH, "" + Stream.of(mdnValues).mapToInt(String::length).sum());
		HttpRequest httpReq = new HttpRequest(testUrl, testUrl.host, testUrl.path, HttpMethod.GET.toString(), "1.1", CollectionUtils.toSingletonList(contentLength), body);
		assertTrue(httpReq.hasData());

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		httpReq.serializeTo(baos);

		String bodyEsperado = Stream.of(mdnValues).collect(Collectors.joining());
		assertTrue(baos.toString().endsWith(bodyEsperado));
	}

	@Test
	public void contemDadosBodySupplierComConteudoChunked() throws IOException {
		Supplier<byte[]> body = mdnSupplier;
		Pair<String, String> transferChunked = Pair.getPair(Headers.KEYS.TRANSFER_ENCODING, Headers.VALUES.TRANSFER_CHUNKED);
		HttpRequest httpReq = new HttpRequest(testUrl, testUrl.host, testUrl.path, HttpMethod.GET.toString(), "1.1", CollectionUtils.toSingletonList(transferChunked), body);
		assertTrue(httpReq.hasData());

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		httpReq.serializeTo(baos);

		String bodyEsperado = Stream.of(mdnValues).map(s -> s.length() + "\r\n" + s + "\r\n").collect(Collectors.joining()) + "0\r\n\r\n";
		assertTrue(baos.toString().endsWith(bodyEsperado));
	}

	public static void main(String... args) {
		ConnPool connPool = new ConnPool();
		connPool.setReadTimeout(60*1000);
		Arrays.asList(postQuery(), postForm(), postFormData(), postData(), postJson(), postJsonChunked(), getQuery()).forEach(req -> connPool.sendRequest(req, DataContentTest::imprimirResponse));
		while (connPool.hasUntreatedResponse()) {
			connPool.treatNextResponse();
		}
	}

	private static HttpRequest getQuery() {
		HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
		requestBuilder.setUrl("https://httpbin.org/get");

		requestBuilder.addFormField("teste", "valor");
		requestBuilder.addFormField("marm", "ota");
		requestBuilder.addFormField("marmota", null);

		return requestBuilder.build();
	}

	private static HttpRequest postForm() {
		HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
		requestBuilder.setUrl("https://httpbin.org/post");
		requestBuilder.setMethod(HttpMethod.POST);

		requestBuilder.addFormField("teste", "valor");
		requestBuilder.addFormField("marm", "ota");
		requestBuilder.addFormField("marmota", null);

		requestBuilder.setContentType(Headers.VALUES.CONTENT_TYPE_URL_ENCODED);

		return requestBuilder.build();
	}

	private static HttpRequest postQuery() {
		HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
		requestBuilder.setUrl("https://httpbin.org/post");
		requestBuilder.setMethod(HttpMethod.POST);
		requestBuilder.forceForm2Query(true);

		requestBuilder.addFormField("teste", "valor");
		requestBuilder.addFormField("marm", "ota");
		requestBuilder.addFormField("marmota", null);

		return requestBuilder.build();
	}

	private static HttpRequest postFormData() {
		HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
		requestBuilder.setUrl("https://httpbin.org/post");
		requestBuilder.setMethod(HttpMethod.POST);

		requestBuilder.addFormField("teste", "valor");
		requestBuilder.addFormField("marm", "ota");
		requestBuilder.addFormField("marmota", null);

		return requestBuilder.build();
	}

	private static HttpRequest postData() {
		HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
		requestBuilder.setUrl("https://httpbin.org/post");
		requestBuilder.setMethod(HttpMethod.POST);
		requestBuilder.setBody("the quick brown fox jumps over the lazy dog".getBytes());

		return requestBuilder.build();
	}

	private static HttpRequest postJsonChunked() {
		IntIndirection i = new IntIndirection(0);
		String[] parts = { "{", "\"teste\"", ":",  " \"value\"", ", ", "\"array\": [ 1, 2, 3],", "\"transfer\": \"chunked\"", "}"};

		HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
		requestBuilder.setMethod(HttpMethod.POST);
		requestBuilder.setUrl("https://httpbin.org/post");
		requestBuilder.setStringBodySupplier(() -> {
			int idx = i.x;
			i.x++;
			if (idx >= parts.length) {
				return null;
			}
			return parts[idx];
		});
		requestBuilder.setContentType(Headers.VALUES.CONTENT_TYPE_APLICATION_JSON);

		return requestBuilder.build();
	}

	private static HttpRequest postJson() {
		HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
		requestBuilder.setMethod(HttpMethod.POST);
		requestBuilder.setUrl("https://httpbin.org/post");
		requestBuilder.setBody("{\"teste\": \"value\", \"array\": [ 1, 2, 3]}");
		requestBuilder.setContentType(Headers.VALUES.CONTENT_TYPE_APLICATION_JSON);

		return requestBuilder.build();
	}

	private static void imprimirResponse(HttpResponse response) {
		System.out.println("código de saída " + response.getStatusCode());
		System.out.println("body...");
		try {
			IOUtils.copyLarge(response.getResponseBody(), System.out);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
