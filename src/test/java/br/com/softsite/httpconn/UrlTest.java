package br.com.softsite.httpconn;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.com.softsite.httpconn.url.Url;

public class UrlTest {

	@Test
	public void resgataSchemeUrl() {
		Url url = new Url("10.0.0.81:8017//");
		assertEquals("10.0.0.81:8017", url.netloc);
		assertEquals("10.0.0.81", url.host);
		assertEquals("8017", url.port);
	}

	@Test
	public void pathTestes() {
		Url googleLalala = new Url("http://google.com/lalala");
		Url googleComSlash = new Url("http://google.com/");
		Url googleCom = new Url("http://google.com");

		assertEquals("google.com", googleLalala.host);
		assertEquals("google.com", googleComSlash.host);
		assertEquals("google.com", googleCom.host);

		assertEquals("/lalala", googleLalala.path);
		assertEquals("/", googleComSlash.path);
		assertEquals("/", googleCom.path);
	}
}
