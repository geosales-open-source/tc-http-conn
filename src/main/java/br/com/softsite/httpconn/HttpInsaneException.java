package br.com.softsite.httpconn;

public class HttpInsaneException extends RuntimeException {

	private static final long serialVersionUID = -2524079757588669615L;

	public HttpInsaneException(String message) {
		super(message);
	}
}
