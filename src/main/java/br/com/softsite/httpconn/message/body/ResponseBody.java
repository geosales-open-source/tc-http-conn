package br.com.softsite.httpconn.message.body;

import java.io.IOException;
import java.io.InputStream;

public abstract class ResponseBody extends InputStream {

	public abstract boolean isLazy();
	public abstract int size();

	public int discharge() throws IOException {
		if (!isLazy()) {
			return 0;
		}
		int acc = 0, data;
		byte[] buff = new byte[1024*8];
		while ((data = read(buff)) != -1) {
			acc += data;
		}
		return acc;
	}
}
