package br.com.softsite.httpconn.message;

public final class Headers {

	private Headers() {}

	public static final class KEYS {

		private KEYS() {}

		public static final String CONTENT_TYPE = "content-type";
		public static final String CONTENT_LENGTH = "content-length";
		public static final String CONTENT_ENCODING = "content-encoding";

		public static final String USER_AGENT = "user-agent";
		public static final String ACCEPTED_ENCODING = "accept-encoding";
		public static final String TRANSFER_ENCODING = "transfer-encoding";
		public static final String CONNECTION = "connection";
	}

	public static final class VALUES {

		private VALUES() {}

		public static final String CONTENT_TYPE_URL_ENCODED = "application/x-www-form-urlencoded";
		public static final String CONTENT_TYPE_PLAIN_TEXT = "text/plain";
		public static final String CONTENT_TYPE_APLICATION_JSON = "application/json";

		public static final String TRANSFER_CHUNKED = "chunked";
		public static final String CONNECTION_KEEP_ALIVE = "keep-alive";
		public static final String CONNECTION_CLOSE = "close";

		public static final String ENCODING_GZIP = "gzip";
		public static final String ENCODING_DEFLATE = "deflate";
		public static final String ENCODING_IDENTITY = "identity";
		public static final String ENCODING_BR = "br";
		public static final String ENCODING_COMPRESS = "compress";
	}
}
