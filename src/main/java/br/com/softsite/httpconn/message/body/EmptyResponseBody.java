package br.com.softsite.httpconn.message.body;

import java.io.IOException;

public class EmptyResponseBody extends ResponseBody {

	public static final EmptyResponseBody empty = new EmptyResponseBody();

	private EmptyResponseBody() {
	}

	@Override
	public boolean isLazy() {
		return false;
	}

	@Override
	public int size() {
		return 0;
	}

	@Override
	public int read() throws IOException {
		return -1;
	}

	@Override
	public int read(byte[] b) throws IOException {
		return -1;
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		return -1;
	}

}
