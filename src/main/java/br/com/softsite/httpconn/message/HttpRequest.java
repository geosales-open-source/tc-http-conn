package br.com.softsite.httpconn.message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

import br.com.softsite.httpconn.HttpInsaneException;
import br.com.softsite.httpconn.IORuntimeException;
import br.com.softsite.httpconn.stream.ChunkedOutputStream;
import br.com.softsite.httpconn.url.Url;
import br.com.softsite.httpconn.util.PushBackSupplier;
import br.com.softsite.toolbox.Pair;
import br.com.softsite.toolbox.collection.CollectionUtils;
import br.com.softsite.toolbox.stream.Stream;

public class HttpRequest {

	public static final String CRLF = "\r\n";

	private final String version;
	private final String method;
	private final String host;
	private final String path;
	private final Url url;

	private final byte[] body;
	private final PushBackSupplier<byte[]> bodySupplier;
	private BooleanSupplier hasData;
	private final ArrayList<Pair<String, String>> headers;

	private boolean chunkedTransfer;

	/**
	 * Não deve conter "content-length" nem "transfer-encoding: chunked"
	 */
	public HttpRequest(Url url, String host, String path, String method, String version, List<Pair<String, String>> headers) {
		this(url, host, path, method, version, headers, null, null);
	}

	/**
	 * Se fornecido o "body", forneça também o header com "content-length"
	 */
	public HttpRequest(Url url, String host, String path, String method, String version, List<Pair<String, String>> headers, byte[] body) {
		this(url, host, path, method, version, headers, body, null);
	}

	/**
	 * Se fornecido o "bodySupplier", forneça também o header com "content-length" OU "transfer-encoding: chunked"
	 * 
	 * Se for fornecido "transfer-encoding: chunked", cada "get" retornado será enviado como
	 * um chunk distinto.
	 * 
	 * O final do "body" é indicado retornando "null"
	 */
	public HttpRequest(Url url, String host, String path, String method, String version, List<Pair<String, String>> headers, Supplier<byte[]> bodySupplier) {
		this(url, host, path, method, version, headers, null, bodySupplier);
	}

	private HttpRequest(Url url, String host, String path, String method, String version, List<Pair<String, String>> headers, byte[] body, Supplier<byte[]> bodySupplier) {
		this.url = url;
		this.host = host;
		this.path = path;
		this.method = method;
		this.version = version;
		this.headers = headers != null? new ArrayList<>(headers): new ArrayList<>();
		this.body = body;
		if (bodySupplier != null) {
			this.bodySupplier = new PushBackSupplier<>(bodySupplier);
			hasData = () -> {
				BooleanSupplier newHasData;
				byte[] data;
				do {
					data = this.bodySupplier.get();
				} while (data != null && data.length == 0);

				if (data != null) {
					this.bodySupplier.pushBack(data);
					newHasData = () -> true;
				} else {
					newHasData = () -> false;
				}
				this.hasData = newHasData;
				return newHasData.getAsBoolean();
			};
		} else {
			this.bodySupplier = null;
			if (body != null) {
				hasData = () -> true;
			} else {
				hasData = () -> false;
			}
		}

		checkSanity();
	}

	private void checkSanity() {
		boolean hasContentLength = false;
		boolean hasTransferEncodingChunked = false;

		for (Pair<String, String> h: headers) {
			switch (h.getKey().toLowerCase()) {
			case Headers.KEYS.CONTENT_LENGTH:
				hasContentLength = true;
				break;
			case Headers.KEYS.TRANSFER_ENCODING: 
				if (!hasTransferEncodingChunked) {
					String v = h.getValue().toLowerCase();
					hasTransferEncodingChunked = Stream.of(v.split(",")).map(String::trim).anyMatch(Headers.VALUES.TRANSFER_CHUNKED::equals);
				}
				break;
			}
		}

		if (hasContentLength && hasTransferEncodingChunked) {
			throw new HttpInsaneException("Must contain 'transfer-encoding: chunked' OR EXCLUSIVE 'content-length'");
		} else if ((hasContentLength || hasTransferEncodingChunked) && !hasData()) {
			throw new HttpInsaneException("Should have 'transfer-encoding: chunked' OR EXCLUSIVE 'content-length' when sending a body");
		}
		chunkedTransfer = hasTransferEncodingChunked;
	}

	public void serializeTo(OutputStream os) throws IOException {
		writeBytesCRLF(method + " " + path + " " + version, os);
		writeBytesCRLF("Host: " + host, os);

		new Stream<>(headers).map(p -> p.getKey() + ": " + p.getValue()).forEach(header -> writeBytesCRLF(header, os));

		writeBytes(CRLF, os);
		if (!chunkedTransfer) {
			if (body != null) {
				writeBytes(body, os);
			} else if (bodySupplier != null) {
				byte[] bodyBytes;
				while ((bodyBytes = bodySupplier.get()) != null) {
					writeBytes(bodyBytes, os);
				}
			}
		} else {
			try (ChunkedOutputStream chunkedOs = new ChunkedOutputStream(os)) {
				if (body != null) {
					writeBytes(body, chunkedOs);
				} else if (bodySupplier != null) {
					byte[] bodyBytes;
					while ((bodyBytes = bodySupplier.get()) != null) {
						writeBytes(bodyBytes, chunkedOs);
					}
				}
			}
		}
		os.flush();
	}

	public boolean hasData() {
		return hasData.getAsBoolean();
	}

	private void writeBytesCRLF(String string2write, OutputStream os) {
		writeBytes(string2write + CRLF, os);
	}

	private void writeBytes(String stuff, OutputStream os) {
		writeBytes(stuff.getBytes(), os);
	}

	private void writeBytes(byte[] stuff, OutputStream os) {
		try {
			os.write(stuff);
		} catch (IOException e) {
			throw new IORuntimeException(e);
		}
	}

	public String serialize() {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			serializeTo(baos);
		} catch (IOException e) {
			throw new IORuntimeException(e);
		}

		return new String(baos.toByteArray());
	}

	public Url getUrl() {
		return url;
	}

	public String getMethod() {
		return method;
	}

	public List<Pair<String, String>> getHeaders() {
		return CollectionUtils.toImmutableList(headers);
	}

	public Stream<Pair<String, String>> getHeaders(String key) {
		return new Stream<>(headers).filter(p -> key.equalsIgnoreCase(p.getKey()));
	}
}
