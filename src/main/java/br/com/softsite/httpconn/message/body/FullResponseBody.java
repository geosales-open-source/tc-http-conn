package br.com.softsite.httpconn.message.body;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import br.com.softsite.httpconn.IORuntimeException;
import totalcross.sys.Vm;
import totalcross.util.IOUtils;

public class FullResponseBody extends ResponseBody {

	private final byte[] bytes;
	private final int realSize;
	private int readUntil = 0;

	public FullResponseBody(InputStream input) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			IOUtils.copyLarge(input, baos);
		} catch (IOException e) {
			throw new IORuntimeException(e);
		}
		bytes = baos.toByteArray();
		realSize = bytes.length;
	}

	@Override
	public boolean isLazy() {
		return false;
	}

	@Override
	public int size() {
		return realSize;
	}

	@Override
	public int read() throws IOException {
		if (readUntil < realSize) {
			byte b = bytes[readUntil];
			readUntil++;
			return b & 0xff;
		}
		return -1;
	}
	
	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		if (readUntil < realSize) {
			int copied = Math.min(bytes.length - readUntil, len);
			Vm.arrayCopy(bytes, readUntil, b, off, copied);

			readUntil += copied;
			return copied;
		}
		return -1;
	}

}
