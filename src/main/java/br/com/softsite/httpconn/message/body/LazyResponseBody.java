package br.com.softsite.httpconn.message.body;

import java.io.IOException;
import java.io.InputStream;

public class LazyResponseBody extends ResponseBody {

	private InputStream input;

	public LazyResponseBody(InputStream input) {
		this.input = input;
	}

	@Override
	public boolean isLazy() {
		return true;
	}

	@Override
	public int size() {
		return -1;
	}

	@Override
	public int read() throws IOException {
		return input.read();
	}
	
	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		return input.read(b, off, len);
	}

}
