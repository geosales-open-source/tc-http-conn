package br.com.softsite.httpconn.message;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import br.com.softsite.httpconn.HttpMethod;
import br.com.softsite.httpconn.url.QueryBuilder;
import br.com.softsite.httpconn.url.Url;
import br.com.softsite.httpconn.url.UrlBuilder;
import br.com.softsite.toolbox.Pair;
import br.com.softsite.toolbox.stream.Collectors;
import br.com.softsite.toolbox.stream.Stream;
import totalcross.net.URI;

public class HttpRequestBuilder {

	public static boolean workAroundTcStream = true;
	private UrlBuilder url;

	private String version = "HTTP/1.1";
	private HttpMethod method = HttpMethod.GET;

	private String contentType = null;
	private Integer contentLength = null;

	private byte[] body = null;
	private String stringBody = null;
	private QueryBuilder formBody = null;
	private Supplier<byte[]> bodySupplier = null;
	private Supplier<String> bodyStringSupplier = null;
	private boolean bodyUrlProtected = false;
	private boolean useFormAsQuery = false;

	private LinkedHashMap<String, String> headers = new LinkedHashMap<>();

	public HttpRequestBuilder() {
		setKeepAlive(true);

		this.addHeader(Headers.KEYS.ACCEPTED_ENCODING, Headers.VALUES.ENCODING_DEFLATE + ", " + Headers.VALUES.ENCODING_GZIP);
		this.addHeader(Headers.KEYS.USER_AGENT, "tc-http-conn/1.0.0");
	}

	public HttpRequestBuilder(HttpRequestBuilder builder) {
		this.url = new UrlBuilder(builder.url);
		this.version = builder.version;
		this.method = builder.method;

		this.contentType = builder.contentType;
		this.contentLength = builder.contentLength;

		this.body = builder.body;
		this.stringBody = builder.stringBody;
		if (builder.formBody != null) {
			this.formBody = new QueryBuilder(builder.formBody);
		}
		this.bodySupplier = builder.bodySupplier;
		this.bodyStringSupplier = builder.bodyStringSupplier;
		this.bodyUrlProtected = builder.bodyUrlProtected;
		this.useFormAsQuery = builder.useFormAsQuery;

		this.headers = new LinkedHashMap<>(builder.headers);
	}

	public void setUrl(Url url) {
		this.url = new UrlBuilder(url);
	}

	/**
	 * Sobreescreve o atual, não copia
	 */
	public void setUrl(UrlBuilder url) {
		this.url = url;
	}

	public void setUrl(String url) {
		this.url = new UrlBuilder(url);
	}

	public HttpMethod getMethod() {
		return method;
	}

	public void setMethod(HttpMethod method) {
		this.method = method;
	}

	public Url getUrl() {
		return url.build();
	}

	public HttpRequestBuilder getCopy() {
		return new HttpRequestBuilder(this);
	}

	/**
	 * Zerará outras informações de body, como formulário
	 */
	public void setBody(String stringBody) {
		clearBody();
		this.stringBody = stringBody;
	}

	/**
	 * Zerará outras informações de body, como formulário
	 */
	public void setBody(byte[] bytes) {
		clearBody();
		body = bytes;
	}

	/**
	 * Zerará outras informações de body, como formulário
	 */
	public void setBodySupplier(Supplier<byte[]> bodySupplier) {
		clearBody();
		this.bodySupplier = bodySupplier;
	}

	/**
	 * Zerará outras informações de body, como formulário
	 */
	public void setStringBodySupplier(Supplier<String> bodyStringSupplier) {
		clearBody();
		this.bodyStringSupplier = bodyStringSupplier;
	}
	
	/**
	 * Zerará outras informações de body
	 */
	public void addFormField(String key, String value) {
		QueryBuilder q = this.formBody;
		clearBody();
		if (q == null) {
			q = new QueryBuilder();
		}
		q.addField(key, value);
		this.formBody = q;
	}

	/**
	 * Deixando aqui porque esse método pode ser feito de modo mais
	 * esperto
	 * 
	 * Limpa todo o conteúdo a ser enviado, sendo implícito que se deseja
	 * preencher o form com mais informações
	 */
	public void clearForm() {
		clearBody();
	}

	public void clearBody() {
		contentLength = null;

		body = null;
		stringBody = null;
		formBody = null;
		bodySupplier = null;
		bodyStringSupplier = null;
		bodyUrlProtected = false;
	}

	public HttpRequest build() {
		List<Pair<String, String>> headers2Send = getHeaders2Send();
		Url url;
		if (useFormAsQuery || (!method.hasBody() && formBody != null && formBody.size() > 0)) {
			UrlBuilder urlTemp = new UrlBuilder(this.url);
			urlTemp.addQueries(formBody);
			url = urlTemp.build();
		} else {
			url = this.url.build();
		}
		if (body != null) {
			return new HttpRequest(url, url.host, url.pathAndQuery(), method.toString(), version, headers2Send, body);
		} else if (bodySupplier != null) {
			return new HttpRequest(url, url.host, url.pathAndQuery(), method.toString(), version, headers2Send, bodySupplier);
		} else {
			return new HttpRequest(url, url.host, url.pathAndQuery(), method.toString(), version, headers2Send);
		}
	}

	public void addHeader(String key, String value) {
		if (Headers.KEYS.CONTENT_TYPE.equalsIgnoreCase(key)) {
			contentType = value;
		}
		headers.put(key, value);
	}

	public void addHeaders(Map<String, String> headersRequest) {
		new Stream<>(headersRequest.entrySet()).forEach(es -> addHeader(es.getKey(), es.getValue()));
	}

	public void setContentType(String value) {
		addHeader(Headers.KEYS.CONTENT_TYPE, value);
	}

	public void setKeepAlive(boolean keepAlive) {
		String keep;
		if (keepAlive) {
			keep = Headers.VALUES.CONNECTION_KEEP_ALIVE;
		} else {
			keep = Headers.VALUES.CONNECTION_CLOSE;
		}
		addHeader(Headers.KEYS.CONNECTION, keep);
	}

	public void clearHeaders() {
		headers.clear();
	}

	public void forceForm2Query(boolean forceQuery) {
		useFormAsQuery = forceQuery;
	}

	private List<Pair<String, String>> getHeaders() {
		return new Stream<>(headers.entrySet()).map(e -> Pair.getPair(e.getKey(), e.getValue())).collect(Collectors.toList());
	}

	private List<Pair<String, String>> getHeaders2Send() {
		ArrayList<Pair<String, String>> headers = new ArrayList<>(getHeaders());
		if (method.hasBody()) {
			if (body == null && bodySupplier == null && bodyStringSupplier == null) {
				normalizeStringBody2Bytes();
			}
			if (bodySupplier == null && bodyStringSupplier != null) {
				normalizeBodyStringSupplier2BodySupplier();
			}
			if (!bodyUrlProtected && Headers.VALUES.CONTENT_TYPE_URL_ENCODED.equalsIgnoreCase(contentType)) {
				if (body != null) {
					body = URI.encode(new String(body)).getBytes();
					contentLength = body.length;
				} else if (bodySupplier != null) {
					Supplier<byte[]> oldSupplier = this.bodySupplier;
					this.bodySupplier = () -> URI.encode(new String(oldSupplier.get())).getBytes();
				}
				bodyUrlProtected = true;
			} else if (body != null && contentLength == null) {
				contentLength = body.length;
			}
			if (contentLength != null) {
				headers.add(Pair.getPair(Headers.KEYS.CONTENT_LENGTH, "" + contentLength));
			} else if (bodySupplier != null) {
				headers.add(Pair.getPair(Headers.KEYS.TRANSFER_ENCODING, Headers.VALUES.TRANSFER_CHUNKED));
			}
		}
		return headers;
	}

	private void normalizeBodyStringSupplier2BodySupplier() {
		Supplier<String> oldStringSupplier = bodyStringSupplier;
		Supplier<String> protectedStringSupplier;
		if (Headers.VALUES.CONTENT_TYPE_URL_ENCODED.equalsIgnoreCase(contentType) && !bodyUrlProtected) {
			protectedStringSupplier = () -> {
				String x = oldStringSupplier.get();
				if (x == null) {
					return null;
				}
				return URI.encode(x);
			};
			bodyUrlProtected = true;
		} else {
			protectedStringSupplier = oldStringSupplier;
		}
		bodySupplier = () -> {
			String x = protectedStringSupplier.get();
			if (x == null) {
				return null;
			}
			return x.getBytes();
		};
	}

	private void normalizeStringBody2Bytes() {
		if (Headers.VALUES.CONTENT_TYPE_URL_ENCODED.equalsIgnoreCase(contentType)) {
			if (stringBody != null) {
				body = URI.encode(stringBody).getBytes();
			} else if (formBody != null && !useFormAsQuery) {
				body = formBody.getQuery().getBytes();
			}
			bodyUrlProtected = true;
		} else {
			if (stringBody != null) {
				body = stringBody.getBytes();
			} else if (formBody != null && !useFormAsQuery) {
				body = formBody.getForm().getBytes();
			}
		}
		if (body != null) {
			contentLength = body.length;
		}
	}
}
