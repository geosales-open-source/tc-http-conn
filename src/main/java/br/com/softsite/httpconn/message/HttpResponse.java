package br.com.softsite.httpconn.message;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import br.com.softsite.httpconn.IORuntimeException;
import br.com.softsite.httpconn.conn.Conn;
import br.com.softsite.httpconn.message.body.ResponseBody;
import br.com.softsite.httpconn.message.body.EmptyResponseBody;
import br.com.softsite.httpconn.message.body.FullResponseBody;
import br.com.softsite.httpconn.message.body.LazyResponseBody;
import br.com.softsite.httpconn.stream.ChunkedInputStream;
import br.com.softsite.httpconn.stream.TruncatedInputStream;
import br.com.softsite.httpconn.stream.WorkAroundZeroInputStream;
import br.com.softsite.toolbox.Pair;
import br.com.softsite.toolbox.collection.CollectionUtils;
import br.com.softsite.toolbox.stream.Stream;
import totalcross.util.zip.CompressedStream;
import totalcross.util.zip.GZipStream;
import totalcross.util.zip.ZLibStream;

public class HttpResponse {

	public static final int THRESHOLD_LAZY = 1024*1024;
	private static final int EOF = -1;

	private final InputStream input;
	private final HttpRequest req;
	private final Conn conn;

	private final ArrayList<Pair<String, String>> responseHeaders = new ArrayList<>();
	private final Runnable terminouLeitura;
	private int statusCode = 0;
	private String protocolResponseVersion;

	private boolean chunked;
	private Function<InputStream, InputStream> decompress = input -> input;

	private boolean hasContentLength;
	private int contentLength;

	private ResponseBody responseBody;

	public HttpResponse(InputStream input, HttpRequest req, Conn conn, Runnable terminouLeitura) {
		this.input = input;
		this.req = req;
		this.conn = conn;
		this.terminouLeitura = terminouLeitura;

		StringBuilder buff = new StringBuilder();

		lePrimeiraLinha(buff);
		if (statusCode == 0) { 
			responseBody = EmptyResponseBody.empty;
		} else {
			buff.setLength(0);
	
			leCabecalhos(buff);
			buff.setLength(0);
	
			if (hasBody()) {
				preparaCorpo();
			}
		}
	}

	public ResponseBody getResponseBody() {
		return responseBody;
	}

	public String getProtocolResponseVersion() {
		return protocolResponseVersion;
	}

	public Conn getConn() {
		return conn;
	}

	public List<Pair<String, String>> getResponseHeaders() {
		return CollectionUtils.toImmutableList(responseHeaders);
	}

	public int getStatusCode() {
		return statusCode;
	}

	private void lePrimeiraLinha(StringBuilder buff) {
		readLine(buff);
		int primeiraLinha_length = buff.length();
		int lastStop = 0;
		int state = 0;

		for (int i = 0; i < primeiraLinha_length; i++) {
			char c = buff.charAt(i);
			if (c == ' ') {
				String substring = buff.substring(lastStop, i);
				if (state == 0) {
					protocolResponseVersion = substring;
					state = 1;
					lastStop = i + 1;
				} else {
					statusCode = Integer.parseInt(substring);
					return;
				}
			}
		}
	}

	private void leCabecalhos(StringBuilder buff) {
		int linha_length;
		while ((linha_length = readLine(buff).length()) != 0) {
			for (int i = 0; i < linha_length; i++) {
				char c = buff.charAt(i);
				if (c == ':') {
					String key = buff.substring(0, i);
					String value = "";
					i++;
					while (i < linha_length && buff.charAt(i) == ' ') {
						i++;
					}
					if (i < linha_length) {
						value = buff.substring(i);
					}
					responseHeaders.add(Pair.getPair(key, value));

					getTratadorHeader(key).accept(value);
					break;
				}
			}
			buff.setLength(0);
		}
	}

	private Consumer<String> getTratadorHeader(String key) {
		switch (key.toLowerCase()) {
		case Headers.KEYS.TRANSFER_ENCODING:
			return this::avaliaTransferEncoding;
		case Headers.KEYS.CONTENT_LENGTH:
			return this::avaliaContentLength;
		case Headers.KEYS.CONTENT_ENCODING:
			return this::avaliaContentEncoding;
		case Headers.KEYS.CONNECTION:
			return this::avaliaConnection;
		default:
			return s -> {};
		}
	}

	private void avaliaTransferEncoding(String transferEncoding) {
		if (transferEncoding.toLowerCase().contains(Headers.VALUES.TRANSFER_CHUNKED)) {
			chunked = true;
		}
	}

	private void avaliaContentLength(String contentLength) {
		this.contentLength = Integer.parseInt(contentLength);
		this.hasContentLength = true;
	}

	@SuppressWarnings("resource")
	private void avaliaContentEncoding(String contentEncoding) {
		Stream.of(contentEncoding.toLowerCase().split(","))
				.map(String::trim)
				.forEach(encoding -> {
					Function<InputStream, InputStream> oldFunction = this.decompress;
					switch (encoding) {
					case Headers.VALUES.ENCODING_GZIP:
						this.decompress = input -> {
							try {
								return new GZipStream(totalcross.io.Stream.asStream(oldFunction.apply(input)), CompressedStream.INFLATE).asInputStream();
							} catch (IOException e) {
								throw new IORuntimeException(e);
							}
						};
						break;
					case Headers.VALUES.ENCODING_DEFLATE:
						this.decompress = input -> new ZLibStream(totalcross.io.Stream.asStream(oldFunction.apply(input)), CompressedStream.INFLATE).asInputStream();
						break;
					case Headers.VALUES.ENCODING_IDENTITY:
						break;
					case Headers.VALUES.ENCODING_COMPRESS:
					case Headers.VALUES.ENCODING_BR:
					default:
						throw new IORuntimeException("compressão não suportada " + encoding);
					}
				});
	}

	private void avaliaConnection(String value) {
		if (Headers.VALUES.CONNECTION_CLOSE.equalsIgnoreCase(value)) {
			getConn().setClosed();
		}
	}

	private boolean hasBody() {
		// https://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html
		// All responses to the HEAD request method MUST NOT include a message-body
		if ("HEAD".equals(req.getMethod())) {
			return false;
		}
		// https://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html
		// All 1xx (informational), 204 (no content), and 304 (not modified) responses MUST NOT include a message-body
		if (statusCode < 200 || statusCode == 204 || statusCode == 304) {
			return false;
		}
		// https://www.w3.org/Protocols/rfc2616/rfc2616-sec4.html
		// The presence of a message-body in a request is signaled by the inclusion of a Content-Length
		// or Transfer-Encoding header field in the request's message-headers
		if (chunked || hasContentLength) {
			return true;
		}
		return false;
	}

	private void preparaCorpo() {
		if (chunked) {
			// preparar corpo chunked
			InputStream leitorChunked = new ChunkedInputStream(input, terminouLeitura);
			responseBody = new LazyResponseBody(getInputStream(leitorChunked));
		} else if (this.contentLength == 0) {
			responseBody = EmptyResponseBody.empty;
		} else {
			InputStream leitorTruncado = new TruncatedInputStream(input, contentLength, terminouLeitura);
			if (contentLength > THRESHOLD_LAZY) {
				responseBody = new LazyResponseBody(getInputStream(leitorTruncado));
			} else {
				responseBody = new FullResponseBody(getInputStream(leitorTruncado));
			}
		}
	}

	private InputStream getInputStream(InputStream leituraBasica) {
		InputStream decompressedStream = decompress.apply(leituraBasica);
		if (HttpRequestBuilder.workAroundTcStream) {
			return new WorkAroundZeroInputStream(decompressedStream);
		} else {
			return decompressedStream;
		}
	}

	private StringBuilder readLine(StringBuilder buff) {
		int r;
		boolean cr = false;

		while ((r = readChar()) != EOF) {
			byte c = (byte) r;

			if (cr) {
				if (c == '\n') {
					return buff;
				} else {
					buff.append('\r');
					if (c != '\r') {
						buff.append(c);
						cr = false;
					}
				}
			} else if (c == '\r') {
				cr = true;
			} else {
				buff.append((char) c);
			}
		}
		return buff;
	}

	private int readChar() {
		try {
			return input.read();
		} catch (IOException e) {
			throw new IORuntimeException(e);
		}
	}
}
