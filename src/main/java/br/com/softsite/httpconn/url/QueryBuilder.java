package br.com.softsite.httpconn.url;

import java.util.ArrayList;

import br.com.softsite.toolbox.Pair;
import br.com.softsite.toolbox.stream.Collectors;
import br.com.softsite.toolbox.stream.Stream;
import totalcross.net.URI;

public class QueryBuilder {

	public static Pair<String, String> toKeyValue(String q) {
		for (int i = 0; i < q.length(); i++) {
			char c = q.charAt(i);
			if (c == '=') {
				return Pair.getPair(URI.decode(q.substring(0, i)), URI.decode(q.substring(i + 1)));
			}
		}
		return Pair.getPair(URI.decode(q), null);
	}

	public static String toQueryString(Pair<String, String> q) {
		String v = q.getValue();
		String k = URI.encode(q.getKey());

		if (v == null) {
			return k;
		}
		return k + "=" + URI.encode(v);
	}

	public static String toFormString(Pair<String, String> q) {
		String v = q.getValue();
		String k = q.getKey();

		if (v == null) {
			return k;
		}
		return k + "=" + v;
	}

	private ArrayList<Pair<String, String>> queryParam;

	public QueryBuilder() {
		queryParam = new ArrayList<>();
	}

	public QueryBuilder(String query) {
		setQuery(query);
	}

	public QueryBuilder(QueryBuilder queryBuilder) {
		queryParam = new ArrayList<>(queryBuilder.queryParam);
	}

	public void setQuery(String query) {
		if (query != null) {
			queryParam = Stream.of(query.split("&")).map(QueryBuilder::toKeyValue).collect(Collectors.toCollection(ArrayList::new));
		} else if (queryParam != null) {
			queryParam.clear();
		} else {
			queryParam = new ArrayList<>();
		}
	}

	public void addField(String key, String value) {
		queryParam.add(Pair.getPair(key, value));
	}

	public String getQuery() {
		return new Stream<>(queryParam).map(QueryBuilder::toQueryString).collect(Collectors.joining("&"));
	}

	public String getForm() {
		return new Stream<>(queryParam).map(QueryBuilder::toFormString).collect(Collectors.joining("&"));
	}

	public ArrayList<Pair<String, String>> getQueryParam() {
		return queryParam;
	}

	public int size() {
		return queryParam.size();
	}

	public void merge(QueryBuilder queries) {
		queryParam.addAll(queries.queryParam);
	}
}
