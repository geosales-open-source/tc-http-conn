package br.com.softsite.httpconn.url;

import br.com.softsite.toolbox.Toolbox;

public class Url {

	public final String resource;
	public final String query;
	public final String fragment;

	public final String scheme;
	public final String netloc;
	public final String path;

	public final String host;
	public final String port;

	public Url(String url) {
		String[] tokenized = tokenizeUrl(url);
		resource = tokenized[0];
		query = tokenized[1];
		fragment = tokenized[2];

		String[] tokenizedResource = tokenizeResource(resource);
		scheme = tokenizedResource[0];
		netloc = tokenizedResource[1];
		path = tokenizedResource[2];

		if (netloc != null) {
			String[] tokenizedNetloc = tokenizeNetloc(netloc);
			host = tokenizedNetloc[0];
			port = tokenizedNetloc[1];
		} else {
			host = port = null;
		}
	}

	@Override
	public String toString() {
		StringBuilder buff = new StringBuilder(resource);

		if (query != null) {
			buff.append("?").append(query);
		}
		if (fragment != null) {
			buff.append("#").append(fragment);
		}
		return buff.toString();
	}

	public String pathAndQuery() {
		if (query != null) {
			return path + "?" + query;
		}
		return path;
	}

	public boolean defaultPort() {
		return port == null || port.equals(getPortStringForScheme(scheme));
	}

	public static String[] tokenizeUrl(String url) {
		String resource = url;
		String queryString = null;
		String fragment = null;

		boolean startQuery = false;
		boolean startFragment = false;
		int idxQuery = -1;
		int idxFrag = -1;
		for (int i = 0; i < url.length(); i++) {
			char c = url.charAt(i);
			if (c == '?') {
				startQuery = true;
				idxQuery = i + 1;
			}
			if (c == '#') {
				startFragment = true;
				idxFrag = i + 1;
			}

			if (startQuery || startFragment) {
				resource = url.substring(0, i);
				break;
			}
		}

		if (startQuery) {
			queryString = url.substring(idxQuery);
			for (int i = idxQuery; i < url.length(); i++) {
				char c = url.charAt(i);
				if (c == '#') {
					startFragment = true;
					idxFrag = i + 1;
					queryString = url.substring(idxQuery, i);
					break;
				}
			}
		}

		if (startFragment) {
			fragment = url.substring(idxFrag);
		}

		return new String[] { resource, queryString, fragment };
	}

	public static String[] tokenizeResource(String resource) {
		boolean schemePlausible = true;
		int nextStart = 0;		
		String scheme = null;
		String netloc = null;
		String path = resource;

		int resourceLength = resource.length();

		for (int i = 0; i < resourceLength; i++) {
			char c = resource.charAt(i);
			if (c == ':') {
				if (i + 2 < resourceLength && resource.charAt(i + 1) == '/' && resource.charAt(i + 2) == '/') {
					scheme = resource.substring(0, i);
					path = resource.substring(i);
					nextStart = i + 1;
				} else {
					schemePlausible = false;
				}
				break;
			}
			if (c == '/') {
				nextStart = i;
				break;
			}
		}
		if (nextStart + 1 < resourceLength) {
			boolean goon = false;
			if (!schemePlausible) {
				goon = true;
			} else if (resource.charAt(nextStart) == '/' && resource.charAt(nextStart + 1) == '/') {
				// achou netloc
				nextStart += 2;
				goon = true;
			}
			if (goon) {
				boolean foundSlash = false;
				for (int i = nextStart; i < resourceLength; i++) {
					char c = resource.charAt(i);
					if (c == '/') {
						netloc = resource.substring(nextStart, i);
						path = resource.substring(i);
						foundSlash = true;
						break;
					}
				}
				if (!foundSlash) {
					netloc = resource.substring(nextStart);
					path = null;
				}
			}
		}

		return new String[] { scheme, netloc, Toolbox.elvis(path, "/") };
	}

	public static String[] tokenizeNetloc(String netloc) {
		String host = netloc;
		String port = null;
		for (int i = 0; i < netloc.length(); i++) {
			char c = netloc.charAt(i);
			if (c == ':') {
				host = netloc.substring(0, i);
				if (i + 1 <= netloc.length()) {
					port = netloc.substring(i + 1);
				}
				break;
			}
		}
		return new String[] { host, port };
	}

	public static String getPortStringForScheme(String scheme) {
		return "" + getPortForScheme(scheme);
	}
	
	public static int getPortForScheme(String scheme) {
		if (scheme == null) {
			scheme = "http";
		}
		switch (scheme.toLowerCase()) {
		case "https": return 443;
		case "http": return 80;
		}
		return 80;
	}
}
