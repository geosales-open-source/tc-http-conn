package br.com.softsite.httpconn.url;

public class UrlBuilder {

	private QueryBuilder queryBuilder;
	private String fragment;

	private String scheme;
	private String path;

	private String host;
	private String port;

	public UrlBuilder(String url) {
		String[] tokenized = Url.tokenizeUrl(url);
		String resource = tokenized[0];
		String query = tokenized[1];
		fragment = tokenized[2];
		queryBuilder = new QueryBuilder();

		setQuery(query);

		String[] tokenizedResource = Url.tokenizeResource(resource);
		scheme = tokenizedResource[0];
		String netloc = tokenizedResource[1];
		path = tokenizedResource[2];

		if (netloc != null) {
			String[] tokenizedNetloc = Url.tokenizeNetloc(netloc);
			host = tokenizedNetloc[0];
			port = tokenizedNetloc[1];
		} else {
			host = port = null;
		}
	}

	public UrlBuilder(Url url) {
		queryBuilder = new QueryBuilder();
		setQuery(url.query);

		fragment = url.fragment;

		scheme = url.scheme;
		path = url.path;

		host = url.host;
		port = url.port;
	}

	public UrlBuilder(UrlBuilder url) {
		this.fragment = url.fragment;

		this.scheme = url.scheme;
		this.path = url.path;

		this.host = url.host;
		this.port = url.port;

		this.queryBuilder = new QueryBuilder(url.queryBuilder);
	}

	public String getFragment() {
		return fragment;
	}

	public void setFragment(String fragment) {
		this.fragment = fragment;
	}

	public String getScheme() {
		return scheme;
	}

	public void setScheme(String scheme) {
		this.scheme = scheme;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		if (path == null) {
			this.path = null;
		} else if (path.charAt(0) != '/') {
			this.path = "/" + path;
		} else {
			this.path = path;
		}
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getQuery() {
		return queryBuilder.getQuery();
	}

	public void addQueries(QueryBuilder queries) {
		this.queryBuilder.merge(queries);
	}

	public void setQuery(String query) {
		queryBuilder.setQuery(query);
	}

	@Override
	public String toString() {
		StringBuilder buff = new StringBuilder();
		if (scheme != null) {
			buff.append(scheme)
				.append(":");
		}
		if (host != null) {
			buff.append("//")
				.append(host);
			if (!defaultPort()) {
				buff.append(':')
				.append(port);
			}
		}

		buff.append(pathAndQuery());
		if (fragment != null) {
			buff.append("#").append(fragment);
		}
		return buff.toString();
	}

	public String pathAndQuery() {
		String queryString = "";
		if (queryBuilder.size() != 0) {
			queryString = "?" + getQuery();
		}
		if (path != null) {
			return path + queryString;
		} else {
			return queryString;
		}
	}

	public boolean defaultPort() {
		return port == null || port.equals(Url.getPortStringForScheme(scheme));
	}

	public Url build() {
		return new Url(this.toString());
	}
}
