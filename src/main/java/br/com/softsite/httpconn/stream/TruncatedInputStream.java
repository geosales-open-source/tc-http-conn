package br.com.softsite.httpconn.stream;

import java.io.IOException;
import java.io.InputStream;

public class TruncatedInputStream extends InputStream {

	private int contentRead = 0;
	private final int contentLength;
	private final InputStream input;
	private final Runnable terminouLeitura;

	public TruncatedInputStream(InputStream input, int contentLength) {
		this(input, contentLength, () -> {});
	}

	public TruncatedInputStream(InputStream input, int contentLength, Runnable terminouLeitura) {
		this.input = input;
		this.contentLength = contentLength;
		this.terminouLeitura = terminouLeitura;
	}

	@Override
	public int read() throws IOException {
		if (contentRead < contentLength) {
			contentRead++;
			int r = input.read();
			if (r == -1) {
				terminouLeitura.run();
			}
			return r;
		}
		return -1;
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		if (contentRead < contentLength) {
			int copied = Math.min(contentLength - contentRead, len);
			int r = input.read(b, off, copied);
			
			if (r == -1) {
				contentRead = contentLength;
				terminouLeitura.run();
				return r;
			}
			contentRead += r;
			
			if (contentRead >= contentLength) {
				terminouLeitura.run();
			}
			return r;
		} else {
			return -1;
		}
	}
}
