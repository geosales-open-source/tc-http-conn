package br.com.softsite.httpconn.stream;

import java.io.IOException;
import java.io.InputStream;

public class WorkAroundZeroInputStream extends InputStream {

	public static final int THRESHOLD_EOF = 100;

	private final byte[] _1byte = new byte[1];
	private final InputStream input;
	private int qt0 = 0;

	public WorkAroundZeroInputStream(InputStream input) {
		this.input = input;
	}

	@Override
	public int read() throws IOException {
		int r = read(_1byte);
		return r > 0? _1byte[0]: r;
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		int r = input.read(b, off, len);
		return tratarBytesLidos(r);
	}

	private int tratarBytesLidos(int r) {
		if (r == 0) {
			qt0++;
			if (qt0 > THRESHOLD_EOF) {
				return -1;
			}
		} else {
			qt0 = 0;
		}
		return r;
	}
}
