package br.com.softsite.httpconn.stream;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import br.com.softsite.toolbox.stream.Collectors;
import br.com.softsite.toolbox.stream.Stream;

public class TeeOutputStream extends OutputStream {

	private final List<OutputStream> tee;

	public TeeOutputStream(OutputStream... dst) {
		this.tee = Stream.of(dst).collect(Collectors.toList());
	}

	@Override
	public void write(int b) throws IOException {
		for (OutputStream o: tee) {
			o.write(b);
		}
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		for (OutputStream o: tee) {
			o.write(b, off, len);
		}
	}

	@Override
	public void flush() throws IOException {
		for (OutputStream o: tee) {
			o.flush();
		}
	}
}
