package br.com.softsite.httpconn.stream;

import java.io.IOException;
import java.io.InputStream;

import totalcross.sys.Vm;

public class ChunkedInputStream extends InputStream {

	private final InputStream input;
	private final Runnable fimLeitura;

	private byte[] buffer = new byte[8*1024];
	private int buff_ini, buff_size;
	private boolean eof = false;
	private boolean endOfChunkedData = false;

	public ChunkedInputStream(InputStream input) {
		this(input, () -> {});
	}

	public ChunkedInputStream(InputStream input, Runnable fimLeitura) {
		this.input = input;
		this.fimLeitura = fimLeitura;
	}

	@Override
	public int read() throws IOException {
		ensureChunk();
		if (hasByteAvailable()) {
			byte b = buffer[buff_ini];
			buff_ini++;
			return b & 0xff;
		} else {
			return -1;
		}
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		ensureChunk();
		if (hasByteAvailable()) {
			int copied = Math.min(buff_size - buff_ini, len);
			Vm.arrayCopy(buffer, buff_ini, b, off, copied);
			
			buff_ini += copied;
			return copied;
		} else {
			return -1;
		}
	}

	private void ensureChunk() throws IOException {
		if (hasByteAvailable() || endOfChunkedData || eof) {
			return;
		}
		int r;
		int size = 0;
		while ((r = input.read()) != -1) {
			if (r == '\r') {
				buff_size = size + 1;
				if (buff_size + 2 > buffer.length) {
					buffer = new byte[(buff_size/1024 + 1)*1024];
				}
				readToBuffer();
				if (size == 0) {
					endOfChunkedData = true;
					fimLeitura.run();
				}
				break;
			} else {
				int v = 0;
				if (r >= '0' && r <= '9') {
					v = r - '0';
				} else if (r >= 'a' && r <= 'f') {
					v = 10 + r - 'a';
				} else {
					v = 10 + r - 'A';
				}
				size = size * 16 + v;
			}
		}
		if (r == -1) {
			eof = true;
			fimLeitura.run();
		}
	}

	private void readToBuffer() throws IOException {
		int accRead = 0;
		int totalLeitura = buff_size + 2;

		buff_ini = 1;

		while (accRead < totalLeitura) {
			int read = input.read(buffer, accRead, totalLeitura - accRead);
			if (read == -1) {
				eof = true;
				fimLeitura.run();
				if (accRead - 1 < buff_size) {
					buff_size = accRead - 1;
				}
				return;
			}
			accRead += read;
		}
	}

	private boolean hasByteAvailable() {
		return buff_size > buff_ini;
	}
}
