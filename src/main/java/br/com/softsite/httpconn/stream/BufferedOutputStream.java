package br.com.softsite.httpconn.stream;

import java.io.IOException;
import java.io.OutputStream;

import totalcross.sys.Vm;

public class BufferedOutputStream extends OutputStream {

	private final OutputStream underlying;
	private final byte[] buff;
	private final int size;
	private int occupied = 0;

	public BufferedOutputStream(OutputStream underlying) {
		this(underlying, 8 * 1024);
	}

	public BufferedOutputStream(OutputStream underlying, int size) {
		this.underlying = underlying;
		this.buff = new byte[size];
		this.size = size;
	}

	@Override
	public void write(int b) throws IOException {
		buff[occupied] = (byte) b;
		occupied++;

		if (occupied == size) {
			flush();
		}
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		int addSize = len;
		int available = size - occupied;

		while (addSize >= available) {
			Vm.arrayCopy(b, off, buff, occupied, available);
			off += available;
			flush();
			available = size;
			addSize = len - off;
		}
		if (addSize > 0) {
			Vm.arrayCopy(b, off, buff, occupied, addSize);
			occupied += addSize;
		}
	}

	@Override
	public void flush() throws IOException {
		underlying.write(buff, 0, occupied);
		occupied = 0;
	}

	@Override
	public void close() throws IOException {
		flush();
	}
}
