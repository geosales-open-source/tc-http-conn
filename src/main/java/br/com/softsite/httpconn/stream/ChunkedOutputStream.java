package br.com.softsite.httpconn.stream;

import java.io.IOException;
import java.io.OutputStream;

import totalcross.sys.Vm;

public class ChunkedOutputStream extends OutputStream {

	private static final byte[] END_CHUNK = new byte[] {'0', '\r', '\n', '\r', '\n'};
	private final OutputStream underlying;
	private byte[] buff;

	public ChunkedOutputStream(OutputStream underlying) {
		this(underlying, 1024);
	}

	public ChunkedOutputStream(OutputStream underlying, int len) {
		this.underlying = underlying;
		buff = new byte[len];
	}

	@Override
	public void write(int b) throws IOException {
		int len = 1 + 2 + 2 + 1;
		ensureBuffSize(len);
		buff[0] = '1';
		buff[1] = '\r';
		buff[2] = '\n';
		buff[3] = (byte) (0xff & b);
		buff[4] = '\r';
		buff[5] = '\n';
		underlying.write(buff, 0, len);
	}

	@Override
	public void write(byte[] b) throws IOException {
		write(b, 0, b.length);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		if (len == 0) {
			return;
		}
		StringBuilder hexRep = new StringBuilder();
		int remaining = len;
		int hexRepLen = 0;

		while (remaining > 0) {
			int dig = remaining % 16;
			remaining /= 16;
			hexRep.append(tochar(dig));
			hexRepLen++;
		}
		int chunkedLen = hexRepLen + len + 4;
		ensureBuffSize(chunkedLen);
		Vm.arrayCopy(hexRep.reverse().toString().getBytes(), 0, buff, 0, hexRepLen);

		buff[hexRepLen] = '\r';
		buff[hexRepLen + 1] = '\n';

		Vm.arrayCopy(b, off, buff, hexRepLen + 2, len);

		buff[hexRepLen + len + 2] = '\r';
		buff[hexRepLen + len + 3] = '\n';

		underlying.write(buff, 0, chunkedLen);
	}

	@Override
	public void flush() throws IOException {
		underlying.flush();
	}

	/**
	 * Não fecha a underlying stream
	 */
	@Override
	public void close() throws IOException {
		super.close();
		underlying.write(END_CHUNK, 0, 5);
	}

	private char tochar(int dig) {
		if (dig < 10) {
			return (char) ('0' + dig);
		}
		return (char) ('a' + dig);
	}

	private void ensureBuffSize(int len) {
		if (buff.length < len) {
			buff = new byte[len - (len%1024) + 1024];
		}
	}
}
