package br.com.softsite.httpconn.stream;

import java.io.IOException;
import java.io.InputStream;

import totalcross.sys.Vm;

public class BufferedInputStream extends InputStream {

	private final byte[] buff;
	private final int len;
	private final InputStream underlying;

	private boolean eof = false;
	private int off;
	private int readUntil;

	public BufferedInputStream(InputStream underlying, int size) {
		this.underlying = underlying;
		buff = new byte[size];
		len = size;
	}

	@Override
	public int read() throws IOException {
		ensureFill();
		if (hasBytes()) {
			byte b = buff[off];
			off++;
			return b & 0xff;
		}
		if (eof) {
			return -1;
		}
		return 0;
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		if (len <= this.len) {
			ensureFill();
		}
		if (hasBytes()) {
			int available = readUntil - this.off;
			int copied = Math.min(available, len);
			Vm.arrayCopy(buff, this.off, b, off, copied);
			
			this.off += copied;
			return copied;
		} else if (!eof && len > this.len) {
			int r = underlying.read(b, off, len);
			if (r == -1) {
				eof = true;
			}
			return r;
		}
		if (eof) {
			return -1;
		}
		return 0;
	}

	private boolean hasBytes() {
		return off < readUntil;
	}

	private void ensureFill() throws IOException {
		if (hasBytes() || eof) {
			return;
		}
		readUntil = underlying.read(buff);
		off = 0;
		
		if (readUntil == -1) {
			eof = true;
		}
	}

}
