package br.com.softsite.httpconn;

public enum HttpMethod {
	GET,
	PUT,
	POST,
	DELETE,

	PATCH,
	// For some arcane http methods
	OPTIONS,
	HEAD,
	TRACE,
	CONNECT;

	public boolean hasBody() {
		switch (this) {
		case GET:
		case HEAD:
		case OPTIONS:
		case CONNECT:
		case TRACE:
			return false;
		case POST:
		case PUT:
		case PATCH:
		case DELETE:
		default:
			return true;
		}
	}
}