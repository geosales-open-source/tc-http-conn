package br.com.softsite.httpconn.util;

import java.util.LinkedList;
import java.util.function.Supplier;

public class PushBackSupplier<T> implements Supplier<T> {

	private final Supplier<T> supplier;
	public final LinkedList<T> pushedBack = new LinkedList<>();

	public PushBackSupplier(Supplier<T> supplier) {
		this.supplier = supplier;
	}

	public void pushBack(T el) {
		pushedBack.push(el);
	}

	@Override
	public T get() {
		if (!pushedBack.isEmpty()) {
			return pushedBack.pop();
		}
		return supplier.get();
	}

}
