package br.com.softsite.httpconn;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import br.com.softsite.httpconn.conn.ConnPool;
import br.com.softsite.httpconn.conn.TimeoutHolder;
import br.com.softsite.httpconn.message.Headers;
import br.com.softsite.httpconn.message.HttpRequest;
import br.com.softsite.httpconn.message.HttpRequestBuilder;
import br.com.softsite.httpconn.message.HttpResponse;
import br.com.softsite.httpconn.message.body.ResponseBody;
import br.com.softsite.toolbox.Pair;
import br.com.softsite.toolbox.math.FunctionUtils;
import br.com.softsite.toolbox.stream.Collectors;
import br.com.softsite.toolbox.stream.Stream;
import totalcross.sys.Vm;

public class HttpConn implements AutoCloseable {

	private HttpRequestBuilder requestBuilder;
	private HttpResponse response = null;
	private TimeoutHolder timeouts;
	private ResponseBody responseBody = null;
	private ConnPool connPool;
	private Map<String, String> responseHeaders;

	public HttpConn(String url) {
		requestBuilder = new HttpRequestBuilder();
		requestBuilder.setUrl(url);
		timeouts = new TimeoutHolder();
		connPool = new ConnPool();
	}

	/**
	 * Se não tiver conteúdo a ser enviado, não faz a requisição e retorna nulo
	 * @return a stream de conexão se tiver conteúdo a enviar, ou nulo caso contrário
	 * @throws IOException
	 */
	public InputStream connectIfContent() throws IOException {
		HttpRequest request = getHttpRequest();
		if (request.hasData()) {
			return __connect(request);
		} else {
			return null;
		}
	}

	/**
	 * Faz a conexão HTTP e retorna a stream de conexão
	 * @return a stream de conexão
	 * @throws IOException
	 */
	public InputStream connect() throws IOException {
		return __connect(getHttpRequest());
	}

	private InputStream __connect(HttpRequest request) throws IOException {
		connPool.setTimeouts(timeouts);
		connPool.sendRequest(request, r -> response = r);
		connPool.treatNextResponse();
		responseBody = response.getResponseBody();

		responseHeaders = new Stream<>(response.getResponseHeaders())
				.collect(
						Collectors.groupingBy(
								p -> p.getKey().toLowerCase(), // agrupando pela chave, case insensitive
								Collectors.mapping( // agrupo os valores mapeando para
										Pair::getValue, // o valor
										Collectors.mapping( // ignorando espaços no fim
												String::trim,
												Collectors.filtering( // removendo antes de juntar
														FunctionUtils.negate(String::isEmpty), // as strings vazias
														Collectors.mapping( // apenas por teimosia porque CharSequence não contém isEmpty, e o argumento do joining precisa ser charsequence, e não string
																FunctionUtils::identity,
																Collectors.joining(", ") // daí juntando o que restou separado por vírgulas
														)
												)
										)
								)
						)
				);

		return responseBody;
	}

	public HttpRequest getHttpRequest() {
		return requestBuilder.build();
	}

	public String getUrl() {
		return requestBuilder.getUrl().toString();
	}

	public boolean isHttps() {
		return "https".equals(requestBuilder.getUrl().scheme);
	}

	public void setContentType(String contentType) {
		requestBuilder.setContentType(contentType);
	}

	public HttpMethod getHttpMethod() {
		return requestBuilder.getMethod();
	}

	public void setHttpMethod(HttpMethod httpMethod) {
		requestBuilder.setMethod(httpMethod);
	}

	public void setHeadersRequest(Map<String, String> headersRequest) {
		requestBuilder.clearHeaders();
		requestBuilder.addHeaders(headersRequest);
	}

	public void addHeaderRequest(String key, String value) {
		requestBuilder.addHeader(key, value);
	}

	public Map<String, String> getHeadersResponse() {
		return responseHeaders;
	}

	public int getContentLength() {
		return responseBody.size();
	}

	public String getContentEncoding() {
		return responseHeaders.get(Headers.KEYS.CONTENT_ENCODING);
	}

	public int getResponseCode() {
		return response.getStatusCode();
	}

	@Override
	public void close() throws IOException {
		if (responseBody != null) {
			try {
				// força a leitura até o final, descartando tudo
				responseBody.discharge();
			} finally {
				responseBody.close();
			}
		}
	}

	public void setContent(String content) {
		requestBuilder.setBody(content);
	}

	public void clearForm() {
		requestBuilder.clearForm();
	}

	public void setFormContent(Map<String, String> form) {
		requestBuilder.clearForm();

		new Stream<>(form.entrySet()).map(entry -> Pair.getPair(entry.getKey(), entry.getValue())).forEach(this::__addFormContent);
	}

	public void setFormContentMultiMap(Map<String, List<String>> form) {
		new Stream<>(form.entrySet())
				.map(entry -> Pair.getPair(entry.getKey(), new Stream<>(entry.getValue()).collect(Collectors.joining(", "))))
				.forEach(this::__addFormContent);
	}

	public void addFormContent(String key, String value) {
		__addFormContent(Pair.getPair(key, value));
	}

	public void addFormContent(String formValue) {
		__addFormContent(Pair.getPair(formValue, null));
	}

	public void addFormContent(Pair<String, String> formContent) {
		__addFormContent(formContent);
	}

	@SafeVarargs
	public final void addFormContent(Pair<String, String>... formContents) {
		for (Pair<String, String> formContent: formContents) {
			__addFormContent(formContent);
		}
	}

	private void __addFormContent(Pair<String, String> formContent) {
		requestBuilder.addFormField(formContent.getKey(), formContent.getValue());
	}
	
	public void setForceQuery(boolean forceQuery) {
		requestBuilder.forceForm2Query(forceQuery);
	}

	public void setContent(InputStream is) {
		byte[] buff = new byte[16 * 1024];
		Supplier<byte[]> supplier = () -> {
			try {
				while (true) {
					int r = is.read(buff);
					if (r == -1) {
						return null;
					}
					if (r == 0) {
						continue;
					}
					if (r == buff.length) {
						return buff;
					}
					byte[] tmp = new byte[r];
					Vm.arrayCopy(buff, 0, tmp, 0, r);
					return tmp;
				}
			} catch (IOException e) {
				throw new IORuntimeException(e);
			}
		};
		requestBuilder.setBodySupplier(supplier);
	}

	public Integer getReadTimeOut() {
		return timeouts.readTimeout;
	}

	public void setReadTimeOut(Integer readTimeOut) {
		timeouts.readTimeout = readTimeOut;
	}

	public Integer getWriteTimeOut() {
		return timeouts.writeTimeout;
	}

	public void setWriteTimeOut(Integer writeTimeOut) {
		timeouts.writeTimeout = writeTimeOut;
	}

	public Integer getOpenTimeOut() {
		return timeouts.openTimeout;
	}

	public void setOpenTimeOut(Integer openTimeOut) {
		timeouts.openTimeout = openTimeOut;
	}

}