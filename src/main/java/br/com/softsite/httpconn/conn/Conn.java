package br.com.softsite.httpconn.conn;

import java.io.IOException;
import java.util.LinkedList;
import java.util.function.Consumer;

import br.com.softsite.httpconn.IORuntimeException;
import br.com.softsite.httpconn.message.HttpRequest;
import br.com.softsite.httpconn.message.HttpResponse;
import br.com.softsite.httpconn.stream.BufferedOutputStream;
import br.com.softsite.httpconn.stream.BufferedInputStream;
import br.com.softsite.httpconn.url.Url;
import br.com.softsite.toolbox.Pair;
import br.com.softsite.toolbox.Toolbox;

import totalcross.net.Socket;
import totalcross.net.SocketFactory;
import totalcross.net.ssl.SSLSocket;
import totalcross.net.ssl.SSLSocketFactory;

public class Conn {

	private final ConnPool connPool;

	private Socket socket;
	private BufferedInputStream bufferedStream;
	private final String scheme;
	private final String host;
	private final int port;

	private Integer openTimeout;
	private Integer readTimeout;
	private Integer writeTimeout;
	private boolean closed = false;
	private boolean mayReadNextResponse = true;
	private boolean defaultPort = false;

	private LinkedList<Pair<HttpRequest, Consumer<HttpResponse>>> requests = new LinkedList<>();

	public Conn(String schemeAndAuthority) {
		this(schemeAndAuthority, new ConnPool());
	}

	public Conn(String schemeAndAuthority, ConnPool connPool) {
		this.connPool = connPool;
		String[] tokenizedRes = Url.tokenizeResource(schemeAndAuthority);
		this.scheme = Toolbox.elvis(tokenizedRes[0], "http");
		String[] tokenizedNetloc = Url.tokenizeNetloc(tokenizedRes[1]);
		this.host = tokenizedNetloc[0];

		int schemePort = Url.getPortForScheme(scheme);
		if (tokenizedNetloc[1] != null) {
			this.port = Integer.parseInt(tokenizedNetloc[1]);
			this.defaultPort = this.port == schemePort;
		} else {
			this.port = schemePort;
			this.defaultPort = true;
		}
		connPool.register(this);
	}

	public Conn(String scheme, String host, Integer port) {
		this(scheme, host, port, new ConnPool());
	}

	public Conn(String scheme, String host, Integer port, ConnPool connPool) {
		this.connPool = connPool;
		this.scheme = Toolbox.elvis(scheme, "http");
		this.host = host;

		int schemePort = Url.getPortForScheme(scheme);
		if (port != null) {
			this.port = port;
			this.defaultPort = this.port == schemePort;
		} else {
			this.port = schemePort;
			this.defaultPort = true;
		}
		connPool.register(this);
	}

	public String getSchemeAndAuthority() {
		return scheme + "://" + host + (defaultPort? "": ":" + defaultPort);
	}

	public Integer getReadTimeout() {
		return readTimeout;
	}
	
	public void setReadTimeout(Integer readTimeout) {
		this.readTimeout = readTimeout;
	}
	
	public Integer getWriteTimeout() {
		return writeTimeout;
	}
	
	public void setWriteTimeout(Integer writeTimeout) {
		this.writeTimeout = writeTimeout;
	}
	
	public Integer getOpenTimeout() {
		return openTimeout;
	}
	
	public void setOpenTimeout(Integer openTimeout) {
		this.openTimeout = openTimeout;
	}

	public ConnPool getConnPool() {
		return connPool;
	}

	public boolean isClosed() {
		return closed;
	}

	public Socket getSocket() {
		if (socket == null) {
			socket = createSocket();
		}
		return socket;
	}

	@FunctionalInterface
	interface SocketYielder3 {
		Socket yield(String host, int port, int openTimeout) throws IOException;
	}

	@FunctionalInterface
	interface SocketYielder2 {
		Socket yield(String host, int port) throws IOException;
	}

	protected Socket createSocket() {
		try {
			Socket s;
			SocketYielder2 sy2;
			SocketYielder3 sy3;
			if ("https".equals(scheme)) {
				SocketFactory sf = SSLSocketFactory.getDefault();
				sy3 = sf::createSocket;
				sy2 = (h, p) -> sf.createSocket(h, p, Socket.DEFAULT_OPEN_TIMEOUT);
			} else {
				SocketFactory sf = SocketFactory.getDefault();
				sy3 = sf::createSocket;
				sy2 = sf::createSocket;
			}
			if (openTimeout != null) {
				s = sy3.yield(host, port, openTimeout);
			} else {
				s = sy2.yield(host, port);
			}
			if (readTimeout != null) {
				s.readTimeout = readTimeout;
			}
			if (writeTimeout != null) {
				s.writeTimeout = writeTimeout;
			}
			if (s instanceof SSLSocket) {
				SSLSocket ss = (SSLSocket) s;
				ss.startHandshake();
			}
			return s;
		} catch (IOException e) {
			throw new IORuntimeException(e);
		}
	}

	public void sendRequest(HttpRequest req, Consumer<HttpResponse> tratativa) {
		if (schemaCompatible(req.getUrl())) {
			closed = req.getHeaders("Connection").anyMatch(p -> "close".equalsIgnoreCase(p.getValue()));
			try {
				Socket s = getSocket();
				requests.add(Pair.getPair(req, tratativa));
				req.serializeTo(new BufferedOutputStream(s.asOutputStream()));
			} catch (IOException e) {
				throw new IORuntimeException(e);
			}
		} else {
			getConnPool().sendRequest(req, tratativa);
		}
	}

	public boolean schemaCompatible(Url url) {
		if (url.host == null || !this.host.equalsIgnoreCase(url.host)) {
			return false;
		}

		if (!this.scheme.equalsIgnoreCase(Toolbox.elvis(url.scheme, "http"))) {
			return false;
		}

		if (defaultPort && url.defaultPort()) {
			return true;
		} else {
			return ("" + this.port).equals(url.port);
		}
	}

	public boolean treatNextResponse() {
		if (mayReadNextResponse && requests.size() > 0) {
			BufferedInputStream buffInput = getBufferedInputStream();
			Pair<HttpRequest, Consumer<HttpResponse>> reqTratativa = requests.removeFirst();
			HttpRequest req = reqTratativa.getKey();
			Consumer<HttpResponse> tratativa = reqTratativa.getValue();
			mayReadNextResponse = false;
			tratativa.accept(new HttpResponse(buffInput, req, this, () -> { 
				mayReadNextResponse = true;
				treatNextResponse();
			}));

			return true;
		} else {
			return false;
		}
	}

	private BufferedInputStream getBufferedInputStream() {
		if (bufferedStream == null) {
			bufferedStream = new BufferedInputStream(socket.asInputStream(), 10*1024);
		}
		return this.bufferedStream;
	}

	public boolean hasUntreatedResponse() {
		return requests.size() > 0;
	}

	public void setClosed() {
		this.closed = true;
	}
}
