package br.com.softsite.httpconn.conn;

public class TimeoutHolder {

	public Integer readTimeout;
	public Integer writeTimeout;
	public Integer openTimeout;

	public void copyInto(TimeoutHolder other) {
		this.openTimeout = other.openTimeout;
		this.readTimeout = other.readTimeout;
		this.writeTimeout = other.writeTimeout;
	}
}
