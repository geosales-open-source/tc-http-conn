package br.com.softsite.httpconn.conn;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.function.Consumer;

import br.com.softsite.httpconn.message.HttpRequest;
import br.com.softsite.httpconn.message.HttpResponse;
import br.com.softsite.httpconn.url.Url;

public class ConnPool {

	private LinkedHashMap<String, Conn> conns = new LinkedHashMap<>();
	private LinkedList<Conn> closedConns = new LinkedList<>();
	private final TimeoutHolder timeoutHolder = new TimeoutHolder();

	public void sendRequest(HttpRequest req, Consumer<HttpResponse> tratativa) {
		String schemeAndAuthority = getSchemeAuthority(req.getUrl());

		Conn conn = getConnection(schemeAndAuthority);
		conn.sendRequest(req, tratativa);
	}

	public Conn getConnection(String schemeAndAuthority) {
		Conn conn = conns.get(schemeAndAuthority);
		if (conn == null || conn.isClosed()) {
			if (conn != null) {
				closedConns.add(conn);
			}
			conn = new Conn(schemeAndAuthority, this);
			if (timeoutHolder.openTimeout != null) {
				conn.setOpenTimeout(timeoutHolder.openTimeout);
			}
			if (timeoutHolder.readTimeout != null) {
				conn.setReadTimeout(timeoutHolder.readTimeout);
			}
			if (timeoutHolder.writeTimeout != null) {
				conn.setWriteTimeout(timeoutHolder.writeTimeout);
			}
		}
		return conn;
	}

	public static String getSchemeAuthority(Url url) {
		String scheme = url.scheme;
		String netloc = url.netloc;
		return (scheme != null? scheme : "http") + "://" + (netloc != null? netloc: "");
	}

	public void register(Conn conn) {
		register(conn.getSchemeAndAuthority(), conn);
	}

	public void register(String schemeAndAuthority, Conn conn) {
		conns.put(schemeAndAuthority, conn);
	}

	public void treatNextResponse() {
		boolean houveTratativa;
		do {
			houveTratativa = false;

			ListIterator<Conn> closedConnIterator = closedConns.listIterator();
			while (closedConnIterator.hasNext()) {
				Conn conn = closedConnIterator.next();
				if (conn.hasUntreatedResponse()) {
					if (conn.treatNextResponse()) {
						houveTratativa = true;
						closedConnIterator.remove();
					}
				} else {
					closedConnIterator.remove();
				}
			}
			for (Conn conn: conns.values()) {
				if (conn.treatNextResponse()) {
					houveTratativa = true;
				}
			}
		} while (houveTratativa);
	}

	public boolean hasUntreatedResponse() {
		boolean untreatedResponse = false;
		ListIterator<Conn> closedConnIterator = closedConns.listIterator();
		while (closedConnIterator.hasNext()) {
			Conn conn = closedConnIterator.next();
			if (conn.hasUntreatedResponse()) {
				untreatedResponse = true;
			} else {
				closedConnIterator.remove();
			}
		}
		if (untreatedResponse) {
			return true;
		}
		for (Conn conn: conns.values()) {
			if (conn.hasUntreatedResponse()) {
				return true;
			}
		}
		return untreatedResponse;
	}

	public void setOpenTimeout(Integer timeout) {
		timeoutHolder.openTimeout = timeout;
	}

	public void clearOpenTimeout() {
		timeoutHolder.openTimeout = null;
	}

	public void setReadTimeout(Integer timeout) {
		timeoutHolder.readTimeout = timeout;
	}

	public void clearReadTimeout() {
		timeoutHolder.readTimeout = null;
	}

	public void setWriteTimeout(Integer timeout) {
		timeoutHolder.writeTimeout = timeout;
	}

	public void clearWriteTimeout() {
		timeoutHolder.writeTimeout = null;
	}

	public void setTimeouts(TimeoutHolder timeouts) {
		timeoutHolder.copyInto(timeouts);
	}
}
